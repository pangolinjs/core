# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [6.0.0-beta.1](https://github.com/pangolinjs/core/compare/v6.0.0-beta.0...v6.0.0-beta.1) (2020-09-18)


### Bug Fixes

* Set correct public asset path for Fractal ([ea990ae](https://github.com/pangolinjs/core/commit/ea990ae8d8a9dbf447b250a262356bf3e974b88f))

## [6.0.0-beta.0](https://github.com/pangolinjs/core/compare/v6.0.0-alpha.5...v6.0.0-beta.0) (2020-09-18)


### Features

* Expose engine configuration ([1eaf009](https://github.com/pangolinjs/core/commit/1eaf009c027eb0fcf6d5bc9d0f06fdaf90ced60d))
* Expose webpack configuration ([48872dd](https://github.com/pangolinjs/core/commit/48872dd90e2cfc987df256b933e1838a6d4c2486))
* Serve UI and files from a single port ([dfd040e](https://github.com/pangolinjs/core/commit/dfd040e992dcee6b498e7e62fc0d0f324d6301a1))

## [6.0.0-alpha.5](https://github.com/pangolinjs/core/compare/v6.0.0-alpha.4...v6.0.0-alpha.5) (2020-09-09)


### Bug Fixes

* Switch to new postcss-loader options format ([77df2a7](https://github.com/pangolinjs/core/commit/77df2a76361213ecdebd220c3d82687579af023c))

## [6.0.0-alpha.4](https://github.com/pangolinjs/core/compare/v6.0.0-alpha.3...v6.0.0-alpha.4) (2020-09-09)


### Features

* Resolve CSS URLs relative to file paths ([c1338cf](https://github.com/pangolinjs/core/commit/c1338cfbd49fd3a6c703aff0fccd85ae6677fdc6))


### Bug Fixes

* Scope Pangolin's Nunjucks head tag ([8d66559](https://github.com/pangolinjs/core/commit/8d665592b0dc7549489c6d8e2664380711fbbfb7))

## [6.0.0-alpha.3](https://github.com/pangolinjs/core/compare/v6.0.0-alpha.2...v6.0.0-alpha.3) (2020-09-08)


### Features

* Generate relative assets paths ([3941979](https://github.com/pangolinjs/core/commit/394197936795a022c5f55d314c8886a630ea913d))
* Introduce separate "static" command ([1e9ced4](https://github.com/pangolinjs/core/commit/1e9ced4b43476a3a126073cf1cbfc30c25213f6c))
* Separate "static" command isn't necessary with relative URLs ([de7f405](https://github.com/pangolinjs/core/commit/de7f4052f250cea01018fc5554ce06e3bc95c88b))

## [6.0.0-alpha.2](https://github.com/pangolinjs/core/compare/v6.0.0-alpha.1...v6.0.0-alpha.2) (2020-09-07)

## [6.0.0-alpha.1](https://github.com/pangolinjs/core/compare/v6.0.0-alpha.0...v6.0.0-alpha.1) (2020-09-07)


### Features

* Implement file name hashing ([a32d8cb](https://github.com/pangolinjs/core/commit/a32d8cb9a6906f23d921fc93754e64c3dfa5985e))

## [6.0.0-alpha.0](https://github.com/pangolinjs/core/compare/v5.8.2...v6.0.0-alpha.0) (2020-08-11)


### Features

* Setup v6 ([5207092](https://github.com/pangolinjs/core/commit/5207092a5526dcc1b40e6b9b3057309d7ff60a22))
